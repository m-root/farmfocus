farmfocus
================

[![Build Status](https://travis-ci.org/exactlyaaron/farm-manager.svg?branch=master)](https://travis-ci.org/exactlyaaron/farm-manager)


farmfocus is a web-application built on Ruby on Rails designed to make tracking of farm operations effortless. Use farmfocus to track your spraying records, acreage cost, and harvest yield income.

Ruby on Rails
-------------

This application requires:

- Ruby 2.3.1p112
- Rails 5

Other Technologies
------------------
* HAML markup
* Bootstrap 3
* jQuery
* Sass
* RSpec/Capybara testing suite
* Postgres database
* Devise/OmniAuth
